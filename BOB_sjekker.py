#!/usr/bin/env python3

import json
import re
import smtplib
import sys
from email.mime.text import MIMEText

import requests
from bs4 import BeautifulSoup as bs4

url = "http://www.bob.no/for-deg/boligmarkedet/ledige-leiligheter"

SMTP             = "smtp.domene.no"
SMTP_PORT         = "587"
SMTP_USERNAME     = "AzureDiamond"
SMTP_PASSWORD     = "hunter2"
sender             = "bruker@eksempel.no"

# For å sende epost til en adresse
#receiver = "bruker@eksempel.no"

# For å sende til to adresser
receivers = ["bruker1@eksempel.no", "bruker2@eksempel.no"]


def send_mail(message):
    # Fjerner unødvendige newlines
    message = re.sub(r'(?<!\n)\n(?!\n)|\n{3,}', '', message)

    msg = MIMEText(message, _charset="utf-8")
    msg['Subject'] = "Oppdaget forskjell pa {}".format(url)
    msg['From'] = sender

    s = smtplib.SMTP(SMTP, SMTP_PORT)
    s.login(SMTP_USERNAME, SMTP_PASSWORD)

    # For å sende til en
    #msg['To'] = receiver
    #s.sendmail(sender, receiver, msg.as_string())

    # For å sende til to
    for receiver in receivers:
        msg['To'] = receiver
        s.sendmail(sender, receiver, msg.as_string())

# Last ned siden, kræsj hvis vi ikke får HTTP/200,
# parse HTMLen og find tabellen med leilighetene.
r = requests.get(url)
r.raise_for_status()
soup = bs4(r.text, "html.parser")
kolB = soup.find('div', {'class' : 'kolB'})

# Finn lengden på teksten med leilighetene
length = len(kolB.text)

# Hvis scriptet kjøres med ekstra argumenter, sa avslutter vi og printer

# JSON med nåværende lengde på teksten
if len(sys.argv) != 1:
    sys.exit('{{"current": {}}}'.format(length))

with open('len.json', 'r') as f:
    data = json.load(f)

# Hvis det er forskjell
if length != data['current']:
    print('Forskjellig lengde på tekst. Sender epost.')
    send_mail(kolB.text)
    print('Epost sendt!')

    # Hvis vi kom så langt, så klarte vi sikkert å sende eposten,
    # og da kan vi også skrive den nye verdien til JSON filen
    data['current'] = length
    with open('len.json', 'w') as f:
        json.dump(data, f)
