# BOB-sjekker
Et program skrevet for å i utgangspunktet følge med på nye leiligheter på Bergen og Omegn sine nettsider.
Programmet sjekker siden en gang i timen og sender epost til brukerene med informasjon i den nye annonse, hvis innholdet på siden har endret siden.

En gang i 2016 så gikk BOB over til å annonsere alt på [Finn](http://m.finn.no/realestate/lettings/search.html?q=bob%20boligutleie) så denne har blitt mindre nyttig nå

## Oppsett
1. Sett opp epostserveren
2. Sett opp hvem som skal motta epost
3. Bestem nettsiden du overvåker
4. Finn div class-navnet og bytt ut kolB med det du fant
5. Evt sett opp en cron job for å få det til å sjekke regelmessig


## Andre bruksområder

* Se etter nye "Dry aged dager" hos Trancher ( https://trancher.no/bergen/dryaged/, div name  = avia_textblock)
* Se etter annonsering av stillingsannonser ( https://www.statoil.com/en/careers/corporate-graduate-programme-promo.html, div name = text parbase section)

### Krav
- Python3
- `pip3 install Beautifulsoup4`
- `pip3 install requests`
